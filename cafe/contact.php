<!DOCTYPE html>
<html>
<head>
    <title>Contact - Cafe</title>
    <link rel="stylesheet" type="text/css" href="css/stylemenu.css">
</head>
<body>
    <header>
        <h1>Welcome to Cafe G3R</h1>
        <nav>
            <ul>
            <li><a href="index.html">Home</a></li>
                <li><a href="menu.php">Menu</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="order.html">Order</a></li>
                <li><a href="order_list.php">Order List</a></li>
            </ul>
        </nav>
    </header>
    <section>
        <h2>hubungi kami</h2>
        <p>anda bisa menghubungi admin dengan menghubungi kontak berikut:</p>
        <p>Email: kelompok1@cafe.com</p>
        <p>Phone: 089509199199</p>
        <br></br>
        <p>lokasi cafe :</p>
        <p>JL.gunung agung no. 7, Malang</p>

    </section>
    <footer>
        <p>&copy; G3R.</p>
    </footer>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>Menu - Cafe</title>
    <link rel="stylesheet" type="text/css" href="css/stylemenu.css">
</head>
<body>
    <header>
        <h1>Welcome to Cafe G3R</h1>
        <nav>
            <ul>
            <li><a href="index.html">Home</a></li>
                <li><a href="menu.php">Menu</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="order.html">Order</a></li>
                <li><a href="order_list.php">Order List</a></li>
            </ul>
        </nav>
    </header>
    <section>
        <h2>Our Menu</h2>
        <ul>
        <?php
    $conn = mysqli_connect('localhost', 'root', '', 'cafe');

    if (!$conn) {
    die('Koneksi database gagal: ' . mysqli_connect_error());
    }

    $query = "SELECT * FROM menu_items";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) > 0) {
    echo "<ul>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<li>";
        echo "<img src='data:image/jpeg;base64," . base64_encode($row['gambar']) . "' alt='" . $row['name'] . "'>";
        echo "<h3>" . $row['name'] . "</h3>";
        echo "<p>Rp." . $row['price'] . "</p>";
        echo "</li>";
    }
    echo "</ul>";
    } else {
    echo "Tidak ada menu yang tersedia.";
    }
    ?>
        </ul>
    </section>
    <footer>
    <p>&copy; G3R.</p>
    </footer>
</body>
</html>

<?php
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
    $orderId = $_GET["id"];
    $conn = mysqli_connect('localhost', 'root', '', 'cafe');
    if (!$conn) {
        die('Koneksi database gagal: ' . mysqli_connect_error());
    }
    $query = "SELECT * FROM orders WHERE id = $orderId";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) == 1) {
        $order = mysqli_fetch_assoc($result);
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <title>Edit Order - Cafe</title>
            <link rel="stylesheet" type="text/css" href="style.css">
        </head>
        <body>
            <header>
                <h1>Welcome to Cafe G3R</h1>
                <nav>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="menu.php">Menu</a></li>
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="order.html">Order</a></li>
                        <li><a href="order_list.php">Order List</a></li>
                    </ul>
                </nav>
            </header>
            <section>
                <h2>Edit Order</h2>
                <form method="post" action="update_order.php">
                    <input type="hidden" name="id" value="<?php echo $order['id']; ?>">
                    <label for="menu_item">Menu Item:</label>
                    <input type="text" id="menu_item" name="menu_item" value="<?php echo $order['menu_item']; ?>">

                    <label for="quantity">Quantity:</label>
                    <input type="number" id="quantity" name="quantity" min="1" value="<?php echo $order['quantity']; ?>">

                    <input type="submit" value="Update">
                </form>
            </section>
            <footer>
                <p>&copy; 2023 Cafe. All rights reserved.</p>
            </footer>
        </body>
        </html>
        <?php
    } else {
        echo "Order not found.";
    }
    mysqli_close($conn);
} else {
    header("Location: order_list.php");
    exit();
}
?>

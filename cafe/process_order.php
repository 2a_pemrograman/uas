<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $menuItem = $_POST["menu_item"];
    $quantity = $_POST["quantity"];
    $conn = mysqli_connect('localhost', 'root', '', 'cafe');
    if (!$conn) {
        die('Koneksi database gagal: ' . mysqli_connect_error());
    }
    $stmt = mysqli_prepare($conn, "INSERT INTO orders (menu_item, quantity) VALUES (?, ?)");
    mysqli_stmt_bind_param($stmt, "si", $menuItem, $quantity);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
    echo "Order placed successfully!";
} else {
    header("Location: order.html");
    exit();
}
?>

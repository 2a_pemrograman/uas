<!DOCTYPE html>
<html>
<head>
    <title>Order List - Cafe</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <header>
        <h1>Welcome to Cafe G3R</h1>
        <nav>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="menu.php">Menu</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="order.html">Order</a></li>
                <li><a href="order_list.php">Order List</a></li>
            </ul>
        </nav>
    </header>
    <section>
        <h2>Order List</h2>
        <?php
        $conn = mysqli_connect('localhost', 'root', '', 'cafe');
        if (!$conn) {
            die('Koneksi database gagal: ' . mysqli_connect_error());
        }
        $query = "SELECT orders.id, menu_items.name, orders.quantity, menu_items.price FROM orders INNER JOIN menu_items ON orders.menu_item = menu_items.name";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            echo "<table>";
            echo "<tr><th>ID</th><th>Menu Item</th><th>Quantity</th><th>Price</th><th>Edit</th><th>Hapus</th></tr>";
            $totalHarga = 0; 
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['name'] . "</td>";
                echo "<td>" . $row['quantity'] . "</td>";
                echo "<td>Rp" . $row['price'] . "</td>";
                echo "<td><a href='edit_order.php?id=" . $row['id'] . "'>Edit</a></td>";
                echo "<td><a href='delete_order.php?id=" . $row['id'] . "'>Hapus</a></td>";
                echo "</tr>";
                $totalHarga += $row['quantity'] * $row['price'];
            }
            echo "<tr>";
            echo "<td colspan='3'>Total</td>";
            echo "<td>Rp" . $totalHarga . "</td>";
            echo "</tr>";
            echo "</table>";
        } else {
            echo "No orders found.";
        }
        mysqli_close($conn);
        ?>
    </section>
    <footer>
    <p>&copy; G3R.</p>
    </footer>
</body>
</html>
